package framework.config;

import framework.base.BrowserType;
import framework.utilities.LogUtil;

public class Settings {

    public static String username;
    public static String password;
    public static String excelPath;
    public static String testURL;
    public static BrowserType browserType;
    public static LogUtil logUtil;
}
