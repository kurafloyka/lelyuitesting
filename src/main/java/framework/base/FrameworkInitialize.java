package framework.base;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;

public class FrameworkInitialize extends Base {



    public void initializeBrowser(BrowserType type) {
        File dir = new File(System
                .getProperty("user.dir"));
        switch (type) {


            case Chrome:
                //setup the chromedriver using WebDriverManager
                WebDriverManager.chromedriver().setup();
                //Create Chrome Options
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--test-type");
                chromeOptions.addArguments("--disable-popup-bloacking");
                chromeOptions.addArguments("--incognito");
                chromeOptions.addArguments("download.default_directory", dir.toString());

                //desired capabilies
                DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
                desiredCapabilities.setJavascriptEnabled(true);
                chromeOptions.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

                //Create driver object for Chrome
                DriverContext.webDriver = new ChromeDriver(chromeOptions);
                DriverContext.webDriver.manage().window().maximize();
                break;


            case Firefox:

                WebDriverManager.firefoxdriver().setup();
                DriverContext.webDriver = new FirefoxDriver();
                break;


            case Safari:


                WebDriverManager.safaridriver().setup();
                DriverContext.webDriver = new SafariDriver();
                break;


            case IE:

                WebDriverManager.iedriver().setup();
                DriverContext.webDriver = new InternetExplorerDriver();
                break;


        }

    }

}
