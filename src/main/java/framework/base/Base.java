package framework.base;


import framework.utilities.ExcelUtil;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;

public class Base {

    public static BasePage currentPage;
    public static Logger LOGGER;
    public ExcelUtil excelUtil;

    public <TPage extends BasePage> TPage GetInstance(Class<TPage> page) {
        Object object = PageFactory.initElements(DriverContext.webDriver, page);
        return page.cast(object);
    }

}
