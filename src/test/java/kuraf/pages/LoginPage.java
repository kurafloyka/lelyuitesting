package kuraf.pages;

import framework.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BasePage {


    public LoginPage() {
    }

    @FindBy(how = How.ID, using = "username")
    public WebElement txtUserName;

    @FindBy(how = How.ID, using = "password")
    public WebElement txtPassword;

    @FindBy(how = How.ID, using = "loginBtn")
    public WebElement loginBtn;


    public HomePage login(String username, String password) {


        txtUserName.sendKeys(username);
        txtPassword.sendKeys(password);

        loginBtn.click();
        return GetInstance(HomePage.class);
    }


}