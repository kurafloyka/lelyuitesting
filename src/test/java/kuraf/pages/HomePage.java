package kuraf.pages;

import framework.base.BasePage;
import framework.base.DriverContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class HomePage extends BasePage {
    public HomePage() {

    }

    @FindBy(how = How.XPATH, using = "//*[text()='Divaportal']")
    public WebElement divaPortalText;


    public void divaportalAccount() throws InterruptedException {



        DriverContext.waitElementClickable(divaPortalText);


        DriverContext.scrollToElement(divaPortalText);

        divaPortalText.click();
    }
}