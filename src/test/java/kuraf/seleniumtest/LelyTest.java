package kuraf.seleniumtest;

import framework.config.Settings;
import kuraf.lelypage.LelyHomePage;
import kuraf.lelypage.LelySearchPage;
import kuraf.pages.HomePage;
import kuraf.pages.LoginPage;
import org.junit.Test;

import java.io.File;
import java.io.IOException;


public class LelyTest extends TestInitializeJunit {

   /* @Test
    public void loginTest() throws InterruptedException {
        LOGGER.info("a test message");
        LOGGER.info("Username : " + excelUtil.ReadCell("UserName", 1)
                + " - Password : " + excelUtil.ReadCell("Password", 1));
        currentPage = GetInstance(LoginPage.class);//new LoginPage();
        currentPage = currentPage.As(LoginPage.class)
                .login(Settings.username, Settings.password);
        currentPage.As(HomePage.class).divaportalAccount();

    }*/


    @Test
    public void verifyEachPageContainsHappy() throws InterruptedException {
        //https://www.lely.com
        currentPage = GetInstance(LelyHomePage.class);
        currentPage.As(LelyHomePage.class).searchActivityStart();
        currentPage.As(LelyHomePage.class).setSearchInputAndHitSearchButton();
        currentPage.As(LelyHomePage.class).validationOfEachNews();

    }

    @Test
    public void verifyDownloadedPage() throws InterruptedException, IOException {
        //https://www.lely.com/techdocs

        currentPage= GetInstance(LelySearchPage.class);
        currentPage.As(LelySearchPage.class).showDocumentOnDropdown();
        currentPage.As(LelySearchPage.class).searchDocument();
        currentPage.As(LelySearchPage.class).verifyTheDocumentShow();
        currentPage.As(LelySearchPage.class).verifyTheElementIsDownloaded();


    }


}
