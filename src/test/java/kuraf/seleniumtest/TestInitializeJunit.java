package kuraf.seleniumtest;

import framework.base.DriverContext;
import framework.base.FrameworkInitialize;
import framework.config.ConfigReader;
import framework.config.Settings;
import framework.utilities.ExcelUtil;
import framework.utilities.LogUtil;

import jxl.read.biff.BiffException;
import org.junit.After;
import org.junit.Before;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TestInitializeJunit extends FrameworkInitialize {

    @Before
    public void initialize() throws IOException {

        ConfigReader.populateSettings();

        Settings.logUtil = new LogUtil();
        Settings.logUtil.createLogFile();
        Settings.logUtil.write("Framework initialized...");
        initializeBrowser(Settings.browserType);
        DriverContext.webDriver.get(Settings.testURL);
        LOGGER = LoggerFactory.getLogger(TestInitializeJunit.class);
        try {
            excelUtil = new ExcelUtil(System
                    .getProperty("user.dir") + Settings.excelPath);
        } catch (BiffException e) {
            e.printStackTrace();
        }

    }
    @After
    public void tearDown() {
        //DriverContext.webDriver.quit();
    }
}
