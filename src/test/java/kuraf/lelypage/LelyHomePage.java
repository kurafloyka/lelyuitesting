package kuraf.lelypage;

import framework.base.BasePage;
import framework.base.DriverContext;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class LelyHomePage extends BasePage {

    public LelyHomePage() {
    }

    @FindBy(how = How.XPATH, using = "//*[@class='header-navigation-button__label' and text()='Search']")
    public WebElement searchButton;

    @FindBy(how = How.ID, using = "global-search")
    public WebElement searchInput;

    @FindBy(how = How.XPATH, using = "//button[@class='button button-tertiary' and @type='submit']")
    public WebElement searchStart;

    @FindBy(how = How.CSS, using = ".item-description")
    public List<WebElement> eachNewsList;


    public void searchActivityStart() throws InterruptedException {

        DriverContext.waitElementClickable(searchButton);
        DriverContext.scrollToElement(searchButton);
        searchButton.click();

    }


    public void setSearchInputAndHitSearchButton(){
        DriverContext.waitElementClickable(searchInput);
        DriverContext.scrollToElement(searchInput);
        searchInput.click();
        searchInput.sendKeys("happy");
        searchStart.click();

    }

    public void validationOfEachNews(){


        DriverContext.WaitForElementVisible(eachNewsList.get(0));
        LOGGER.info("Size of List : "+eachNewsList.size());
        for(WebElement webElement : eachNewsList){
            LOGGER.info("Detail of article : " + webElement.getText());
            //I can add asserton
            if(webElement.getText().contains("happy")){
                LOGGER.info("Happy word is contained");
            }else{
                LOGGER.info("Happy word is not contained");
            }
        }
    }
}
