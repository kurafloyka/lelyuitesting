package kuraf.lelypage;

import framework.base.BasePage;
import framework.base.DriverContext;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LelySearchPage extends BasePage {

    public LelySearchPage() {
    }


    @FindBy(how = How.ID, using = "select2-id_q-container")
    public WebElement dropdown;

    @FindBy(how = How.XPATH, using = "//input[@class='select2-search__field']")
    public WebElement dropdownTextInput;

    @FindBy(how = How.XPATH, using = "(//*[contains(text(),'View this document')])[1]")
    public WebElement viewDocumentButton1;

    @FindBy(how = How.XPATH, using = "(//a[@class='button button-secondary icon-pdf'])[2]")
    public WebElement downloadButton2;




    public void showDocumentOnDropdown() throws InterruptedException {

        DriverContext.waitElementClickable(dropdown);
        DriverContext.scrollToElement(dropdown);
        dropdown.click();
    }

    public void searchDocument(){
        DriverContext.waitElementClickable(dropdownTextInput);
        dropdownTextInput.sendKeys("LUNA EUR" + Keys.ENTER);

    }

    public void verifyTheDocumentShow(){
        DriverContext.scrollToElement(viewDocumentButton1);
        DriverContext.WaitForElementVisible(viewDocumentButton1);
        viewDocumentButton1.click();
        DriverContext.openWindowsByIndex(1);
        DriverContext.openWindowsByIndex(0);

    }

    public void verifyTheElementIsDownloaded() throws IOException {

        //capabilitide bir problem var cozecegim onu indirilen path hardcoded.
        FileUtils.deleteDirectory(new File("/Users/farukakyol/Downloads"));
        DriverContext.scrollToElement(downloadButton2);
        DriverContext.WaitForElementVisible(downloadButton2);
        downloadButton2.click();

        File temp;

        temp = File.createTempFile("D-S006VT_", ".pdf");

        boolean exists = temp.exists();
        System.out.println("Temp file exists : " + exists);

        Assert.assertTrue(exists);
    }
}
